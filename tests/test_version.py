from click.testing import CliRunner

from zklint.cli import main


def test_version():
    runner = CliRunner()
    result = runner.invoke(main, ["--version"])
    assert result.exit_code == 0
    assert "version 0.1.0" in result.output
