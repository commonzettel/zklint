from pathlib import Path

from zklint import Zettel, Zettelkasten

BASEDIR = str(Path(__file__).parent / "zk")


def test_zettelkasten():
    zk = Zettelkasten(BASEDIR)
    assert type(zk) is Zettelkasten
    files = [f for f in zk.files()]
    assert len(files) == 3
    assert "ztl/batman.adoc" in files


def test_adler():
    zk = Zettelkasten(BASEDIR)
    z = Zettel(zk, "ztl/adler.md")
    assert z.adler32(seed=1) == "F9491CC0"
    assert not z.is_empty()


def test_empty():
    zk = Zettelkasten(BASEDIR)
    z = Zettel(zk, "ztl/empty.rst")
    assert z.is_empty()
