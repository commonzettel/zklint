from click.testing import CliRunner

from zklint.cli import main


def test_debug():
    runner = CliRunner()
    result = runner.invoke(main, ["--debug", "version"])


#   assert result.exit_code == 0
#   assert "Debugging is ON" in result.output
#   assert "zklint, version 0.1.0" in result.output


def test_no_debug():
    runner = CliRunner()
    result = runner.invoke(main, ["version"])


#   assert result.exit_code == 0
#   assert "Debugging is ON" not in result.output
#   assert "zklint, version 0.1.0" in result.output
