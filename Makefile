.PHONY: clean realclean install format lint test

usage:
	@echo "usage: make [clean|install|format|lint|test]"

clean:
	poetry run ruff clean
	rm -rf .mypy_cache .pytest_cache

realclean: clean
	poetry cache clear --no-interaction --all PyPI
	poetry cache clear --no-interaction --all _default_cache

install:
	poetry install --with dev,test

update:
	poetry update

format:
	poetry run black src/ tests/
	poetry run isort src/ tests/

lint:
	poetry check
	-poetry run ruff check src/ tests/
	-poetry run flake8 src/ tests/
	-poetry run pydocstyle src/ tests/
	-poetry run mypy src/ tests/

run:
	poetry run zklint --help
	poetry run zklint --version

#poetry run zklint --debug

test:
	poetry run pytest -vv
	poetry run coverage run -m --source=src pytest tests
	poetry run coverage report
