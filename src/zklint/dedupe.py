"""Find duplicate or near-duplicate filenames and file contents."""

# future improvements:
#  * find symlinks that point to the same source file
#  * files with similar names are often due to dates (e.g. journal-20220304 vs.
#    journal-20220305) maybe exclude files with numbers from this test?
#  * find files with similar contents

from collections import defaultdict
from difflib import get_close_matches

import click

from zklint import pass_zk
from zklint.cli import main


@click.group()
@pass_zk
@click.pass_context
def dedupe(ctx, zk):
    """Find duplicate data in the zettelkasten."""


@dedupe.command("all")
@click.option(
    "--command",
    is_flag=True,
    show_default=True,
    default=False,
    help="Output console commands to fix the lint.",
)
@click.pass_context
def _all(ctx, command):
    """Run all the deduplication subcommands."""
    ctx.invoke(names)
    ctx.invoke(exact)


@dedupe.command()
@pass_zk
def names(zk):
    """Return a list of filenames that might be duplicates."""

    def contains_digit(string: str) -> bool:
        """Return True if `string` contains a digit."""
        for char in string:
            if char.isdigit():
                return True
        return False

    # get a list of all the files except the numbery ones
    files = [str(f) for f in zk.files()]
    files = [f for f in files if not contains_digit(f)]

    # find potentially matching filenames
    count = 0
    for name in [str(f) for f in files]:
        matches = get_close_matches(name, files, cutoff=0.9)
        if len(matches) > 1:
            click.echo(f"{matches}")
            count += 1

    if count > 1:
        click.secho(f"Found {count} possible filename matches.", fg="red")


@dedupe.command()
@pass_zk
def exact(zk):
    """Look for exactly duplicate Zettel based on checksum."""
    seen = defaultdict(list)

    for ztl in zk.zettel():
        if not ztl.is_empty():
            seen[ztl.adler32()].append(ztl)

    for zettel in seen.values():
        if len(zettel) > 1:
            for z in zettel:
                click.echo(z)
            click.echo()


main.add_command(dedupe)
