"""Generate weekly logfile."""

# import os
# from datetime import date
#
# import click
#
#
# class ZK(object):
#     def __init__(self, home=None, debug=False):
#         self.home = os.path.abspath(home or ".")
#         self.debug = debug
#
#
# @click.group()
# @click.option("--repo-home", envvar="REPO_HOME", default=".repo")
# @click.option("--debug/--no-debug", default=False, envvar="REPO_DEBUG")
# @click.pass_context
# def cli(ctx, repo_home, debug):
#     ctx.obj = ZK(repo_home, debug)
#
#
# def asciidoc_week(dt=date.today()):
#     yyyy = dt.isocalendar()[0]
#     ww = dt.isocalendar()[1]
#
#     days = ""
#     for d in range(1, 8):
#         tmp = date.fromisocalendar(yyyy, ww, d)
#         days += tmp.strftime("== %A, %B %-d\n\n\n\n")
#
#     first = date.fromisocalendar(yyyy, ww, 1).strftime("%a %b %-d %Y")
#     last = date.fromisocalendar(yyyy, ww, 7).strftime("%a %b %-d %Y")
#
#     return f"""= {yyyy} week {ww}
# :description: ISO {yyyy}-W{ww}, {first} - {last}
#
# {days}"""
#
#
# print(asciidoc_week())
