"""Define tools & classes to be used in the package."""

# TO DO:
# * Method zk.walk() has a lot of hardcoded behavior that might be better as
#   optional.

import os
import random
import re
import tomllib
import zlib
from pathlib import Path
from typing import Generator

import click
import pathspec

# I am probably overthinking this
random.seed()
HASHSEED = random.randrange(9999999)


def configure(ctx, param, filename):
    """Read the TOML config file."""
    ctx.default_map = {}
    if filename:
        with filename.open("rb") as conffile:
            try:
                options = tomllib.load(conffile)
            except Exception:
                options = {}
            ctx.default_map.update(options)


def adler32(filepath: str, seed: int = HASHSEED, blksz: int = 65536) -> str:
    """Return the adler32 checksum of <filename>."""
    with open(filepath, "rb") as fh:
        while block := fh.read(blksz):
            seed = zlib.adler32(block, seed)
        return "%08X" % (seed & 0xFFFFFFFF)


class ZKFile:
    """Class for any file in the Zettelkasten."""

    def __init__(self, zk, filename):
        """Initialize the object."""
        self.zk = zk
        self.filename = filename


class Zettel(ZKFile):
    """All yer zettel things go here."""

    def __init__(self, zk=None, filename=None):
        """Initialize the object."""
        self.zk = zk
        self.filename = filename

    def __repr__(self):
        """Return a string represenation of the zettelkasten object."""
        return f"""<{self.filename}>"""

    def __str__(self):
        """Return a string represenation of the zettelkasten object."""
        return f"""<{self.filename}>"""

    def fullpath(self):
        """Return full filesystem path to this zettel."""
        if self.zk is None:
            raise Exception("Can't calculate zkpath; no Zettelkasten object")
        return os.path.join(self.zk.basedir, self.filename)

    def zkpath(self):
        """Return relative Zettelkasten path to this zettel."""
        if self.zk is None:
            raise Exception("Can't calculate zkpath; no Zettelkasten object")

    def adler32(self, **kwargs) -> str:
        """Return a string containing the adler32 checksum of <filename>."""
        return adler32(self.fullpath(), **kwargs)

    def is_empty(self) -> bool:
        """Return True if this file has length zero or is only whitespace."""
        with open(self.fullpath(), mode="rb") as f:
            block = f.read(1024)
            if len(block):
                return True if block.isspace() else False
            else:
                return True

    def tokens(self) -> Generator[str, None, None]:
        """Return all the text tokens from this zettel."""
        # This is really rudimentary, MVP stuff
        with open(self.fullpath(), mode="r") as f:
            for line in f:
                for token in line.split():
                    if re.match(r"^[a-z]+$", token, re.IGNORECASE):
                        yield token.lower()


class Zettelkasten:
    """A Zettelkästen is the box where all the Zettel live."""

    def __init__(self, basedir: str, debug=False):
        """Initialize the object."""
        self.basedir = basedir
        self.debug = debug

    def __str__(self):
        """Return a string represenation of the zettelkasten object."""
        return f"""Zettelkasten(basedir={self.basedir})"""

    def strip_basedir(self, path: str) -> str:
        """Return path minus its basedir prefix, if present."""
        return path.removeprefix(self.basedir).removeprefix("/")

    def zkpath(self, *args) -> str:
        """
        Return a path relative to the zettelkasten root.

        For example:
        >>> basedir = '/tmp/'
        >>> zk = Zettelkasten(basedir)
        >>> zk.zkpath(basedir, 'foo', 'bar', 'README.txt')
        'foo/bar/README.txt'
        """
        fullpath = os.path.join(args[0], *args[1:])
        return fullpath.removeprefix(self.basedir).removeprefix("/")

    def files(self, absolute_paths=False, include_dirs=False):
        """Return all files in the zettelkasten, of all types."""
        for root, dirs, files in self.walk(absolute_paths=absolute_paths):
            for f in files:
                yield str(Path(root, f))

            if include_dirs:
                for d in dirs:
                    yield str(Path(root, d))

    def zettel(self):
        """Return all the zettel in the ZK, as objects."""
        for root, _, files in self.walk(absolute_paths=False):
            for f in files:
                yield Zettel(self, self.zkpath(root, f))

    def gitignore_pathspec(self):
        """Build a pathspec object to ignore files in ".gitignore"."""
        try:
            path = os.path.join(self.basedir, ".gitignore")
            with open(path, mode="r") as gitignore:
                return pathspec.GitIgnoreSpec.from_lines(
                    gitignore.read().splitlines()
                )
        except FileNotFoundError:
            return pathspec.PathSpec.from_lines("gitwildmatch", [])

    def walk(self, absolute_paths=True, relative_paths=False):
        """Walk all files in the Zettelkasten."""
        if relative_paths:
            absolute_paths = False
        for root, dirs, files in os.walk(self.basedir, topdown=True):
            # ignore files in .git/, and files defined in .gitignore
            spec = self.gitignore_pathspec()

            def gitignore(spec, root, f):
                path = self.zkpath(root, f)
                return spec.match_file(path)

            files[:] = [f for f in files if not gitignore(spec, root, f)]
            dirs[:] = [d for d in dirs if d not in [".git"]]

            if absolute_paths:
                yield root, dirs, files
            else:
                yield self.strip_basedir(root), dirs, files


# define a Click decorator to set deep context
pass_zk = click.make_pass_decorator(Zettelkasten, ensure=True)
