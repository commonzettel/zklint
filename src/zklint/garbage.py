"""Preserve some old garbage code I don't know what to do with."""
# import click
# import uuid
# import time
# from datetime import datetime
# @click.group()
# @click.pass_context
# @click.option('--debug/--no-debug', default=False)
# def vv(ctx, debug):
#     click.echo(f"Debug mode is {'on' if debug else 'off'}")
# @vv.command()
# @click.pass_context
# @click.option(
#     "-a",
#     "--algorithm",
#     default="uuid",
#     show_default=True,
#     type=click.Choice(["guid", "uuid", "utc", "ymd"], case_sensitive=False),
#     help="Choose the object hashing algorithm",
# )
# @click.option(
#     "-w",
#     "--write",
#     is_flag=True,
#     default=False,
#     show_default=True,
#     help="Actually write the object into the object database.",
# )
# @click.argument('file', type=click.File('rb'))
# def get_ref(ctx, algorithm, write, file):
#     from pprint import pprint
#     pprint(ctx)
#     if algorithm in ["guid", "uuid"]:
#         ref = 'uuid.' + str(uuid.uuid4())
#     elif algorithm in ["utc"]:
#         ref = 'utc.' + f"{time.time():.3f}"
#     elif algorithm in ["ymd"]:
#         dt = datetime.now()
#         ref = f"ymd.{dt:%Y%m%d.%H%M%S}.{dt.microsecond // 1000:03d}"
#     click.echo(ref)
# vv.add_command(get_ref)
