"""Find sketchy things in the Zettelkasten."""

# find broken symlinks

import pathlib

import click
import filetype

from zklint import Zettelkasten, configure, pass_zk


@click.group()
@click.version_option()
@click.option("--debug/--no-debug", default=False, help="Enable debug output.")
@click.option(
    "-c",
    "--config",
    type=click.Path(dir_okay=False),
    callback=configure,
    is_eager=True,
    expose_value=False,
    help="Read option defaults from the specified TOML file",
    show_default=False,
)
@click.pass_context
def main(ctx, basedir, debug):
    """Define main click group for ZK lint utility."""
    ctx.obj = Zettelkasten(basedir)


@click.group()
@click.option(
    "--command",
    is_flag=True,
    show_default=True,
    default=False,
    help="Output console commands to fix the lint.",
)
@pass_zk
@click.pass_context
def lint(ctx, zk, command):
    """Zettelkasten command group for linting."""


@lint.command(name="all")
@click.option(
    "--command",
    is_flag=True,
    show_default=True,
    default=False,
    help="Output console commands to fix the lint.",
)
@click.pass_context
def _all(ctx, command):
    """Run all the lint subcommands."""
    ctx.invoke(filenames)
    ctx.invoke(filetypes)
    ctx.invoke(permissions)


@lint.command()
@click.option(
    "--command",
    is_flag=True,
    show_default=True,
    default=False,
    help="Output console commands to fix the lint.",
)
@pass_zk
def filetypes(zk, command):
    """Verify that filetypes match file extensions."""
    for f in zk.files(absolute_paths=True):
        suffix = pathlib.Path(f).suffix.removeprefix(".")
        kind = filetype.guess(f)
        if suffix and kind and (suffix != kind.extension):
            print(f"Possible problem: {f} {suffix=} {kind.extension=}")


@lint.command()
@click.option(
    "--command",
    is_flag=True,
    show_default=True,
    default=False,
    help="Output console commands to fix the lint.",
)
@pass_zk
def filenames(zk, command) -> int:
    """Lint filenames."""
    count = 0
    BADCHARS = [" ", "_", ":"]

    def fmt_git_mv(src):
        from shlex import quote

        dest = src.replace(" ", "-").replace("_", "-").replace(":", "-")
        return f"git mv {quote(src)} {quote(dest)}"

    for _root, _dirs, files in zk.walk():
        for f in files:
            if any(x in f for x in BADCHARS):
                count += 1
                lint = (
                    fmt_git_mv(f)
                    if command
                    else f"File name '{f}' contains linty characters"
                )
                click.echo(lint)

    return count


@lint.command()
@click.option(
    "--command",
    is_flag=True,
    show_default=True,
    default=False,
    help="Output console commands to fix the lint.",
)
@pass_zk
def permissions(zk, command) -> int:
    """Lint file permissions."""
    count = 0

    def console_chmod(filename, mode):
        from shlex import quote

        return f"chmod -h {mode:04o} {quote(filename)}"

    # get the full paths to all the files and directories
    for path in zk.files(absolute_paths=True, include_dirs=True):
        mode = path.stat(follow_symlinks=False).st_mode & 0x1FF

        # if this is a dir, it should have mode 0755 or 0750
        if path.is_dir():
            if mode not in [0o755, 0o750]:
                count += 1
                click.echo(f"Directory {path} has perms: {mode:o}")

        # if this file is a symlink, it should have mode 0755
        elif path.is_symlink():
            if mode not in [0o755]:
                count += 1
                lint = (
                    console_chmod(path, 0o755)
                    if command
                    else f"Symlink {path} has perms: {mode:o}"
                )
                click.echo(lint)

        # if this is a regular file, it should have mode 0640 or 0644
        elif path.is_file():
            if mode not in [0o644, 0o640]:
                count += 1
                lint = (
                    console_chmod(path, 0o644)
                    if command
                    else f"File {path} has perms: {mode:o}"
                )

    return count


main.add_command(lint)
