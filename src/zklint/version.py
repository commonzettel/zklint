"""Show the application version."""


import importlib.metadata

import click

from zklint.cli import main


@click.command()
@click.pass_context
def version(ctx, **kwargs):
    """Print the current version."""
    __version__ = importlib.metadata.version("zklint")
    click.echo(f"zklint, version {__version__}")
    if ctx.parent.params.get("debug", False):
        click.echo("Debugging is ON")


main.add_command(version)
