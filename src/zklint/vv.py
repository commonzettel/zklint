"""Define the main command group."""

import tomllib
from importlib import import_module
from pathlib import Path

import click
from vannevar import Zettelkasten
from xdg_base_dirs import xdg_config_home

DEFAULT_CONFIGFILE = xdg_config_home() / "vannevar" / "config.toml"
DEFAULT_BASEDIR = Path.home() / "Documents" / "Zettelkasten"


def configure(ctx, param, filename):
    """Read the TOML config file."""
    ctx.default_map = {}
    with filename.open("rb") as conffile:
        try:
            options = tomllib.load(conffile)
        except Exception:
            options = {}
        ctx.default_map.update(options)


@click.group()
@click.version_option()
@click.option("--debug/--no-debug", default=False, help="Enable debug output.")
@click.option(
    "-c",
    "--config",
    type=click.Path(dir_okay=False),
    default=DEFAULT_CONFIGFILE,
    callback=configure,
    is_eager=True,
    expose_value=False,
    help="Read option defaults from the specified TOML file",
    show_default=False,
)
@click.option(
    "--basedir",
    type=click.Path(exists=True),
    default=DEFAULT_BASEDIR,
    expose_value=True,
    help="Base directory of the Zettelkasten",
    show_default=True,
)
@click.pass_context
def main(ctx, basedir, debug):
    """Define main click group for vannevar utility."""
    ctx.obj = Zettelkasten(basedir)


# import click subcommand modules
import_module("vannevar.dedupe")
import_module("vannevar.stats")
import_module("vannevar.version")
